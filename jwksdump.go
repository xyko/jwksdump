package main

import (
	"context"
	"crypto/x509"
	"encoding/pem"
	"flag"
	"fmt"
	"os"

	"github.com/lestrrat-go/jwx/jwk"
)

var (
	url string
)

func init() {
	flag.StringVar(&url, "url", "", "JWKS URL")
}

func main() {
	flag.Parse()

	if url == "" {
		flag.Usage()
		os.Exit(1)
	}

	ctx := context.Background()

	set, err := jwk.Fetch(ctx, url)
	noerr(err)

	it := set.Iterate(ctx)
	for it.Next(ctx) {
		p := it.Pair()

		jkey := p.Value.(jwk.Key)
		var rawkey interface{}
		jkey.Raw(&rawkey)

		printPEM(jkey.KeyID(), rawkey)
	}
}

func printPEM(id string, key any) {
	blockBytes, err := x509.MarshalPKIXPublicKey(key)
	noerr(err)

	ret := pem.EncodeToMemory(&pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: blockBytes,
	})

	fmt.Printf("KEY ID: %s\n%s\n", id, ret)
}

func noerr(err error) {
	if err != nil {
		panic(err)
	}
}
